package example.micronaut

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Produces
import io.micronaut.http.annotation.QueryValue
import io.micronaut.scheduling.TaskExecutors
import io.micronaut.scheduling.annotation.ExecuteOn
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.withContext
import java.util.concurrent.ExecutorService
import java.util.concurrent.ForkJoinPool
import javax.inject.Named

@Controller("/products")
class ProductController(
        @Named(TaskExecutors.IO) private val executor: ExecutorService = ForkJoinPool.commonPool(),
        private val service: Service
) {
    private val coroutineDispatcher: CoroutineDispatcher

    init {
        coroutineDispatcher = executor.asCoroutineDispatcher()
    }

    @Get("/executor{?productId}")
    @Produces(MediaType.APPLICATION_JSON)
    @ExecuteOn(TaskExecutors.IO)
    suspend fun getProductsWithTaskExecutor(@QueryValue("productId")
                                            productId: String? = null): List<Product> {
        if (productId.isNullOrBlank()) throw BadRequestException("Bad Request")
        return service.getProducts()
    }

    @Get("/dispatcher{?productId}")
    @Produces(MediaType.APPLICATION_JSON)
    suspend fun getProductsWithCoroutineContextDispatcher(@QueryValue("productId")
                                                              productId: String? = null): List<Product> {

        return withContext(coroutineDispatcher) {
            if (productId.isNullOrBlank()) throw BadRequestException("Bad Request")
            service.getProducts()
        }
    }

    @Get("/scope{?productId}")
    @Produces(MediaType.APPLICATION_JSON)
    suspend fun getProductsWithCoroutineRaisedScope(@QueryValue("productId")
                                                        productId: String? = null): List<Product> {
        if (productId.isNullOrBlank()) throw BadRequestException("Bad Request")
        return with(CoroutineScope(coroutineDispatcher)) {
            service.getProducts()
        }
    }
}