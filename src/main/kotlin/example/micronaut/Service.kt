package example.micronaut

import javax.inject.Singleton

interface Service {
    suspend fun getProducts(): List<Product>
}

@Singleton
class DefaultService:Service{
    override suspend fun getProducts(): List<Product> {
        return listOf(Product("1"), Product("2"), Product("3"), Product("4"))
    }

}