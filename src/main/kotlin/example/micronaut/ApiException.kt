package example.micronaut

import io.micronaut.context.annotation.Requires
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpResponseFactory
import io.micronaut.http.HttpResponseProvider
import io.micronaut.http.HttpStatus
import io.micronaut.http.annotation.Produces
import io.micronaut.http.server.exceptions.ExceptionHandler
import javax.inject.Singleton

private const val genericErrorMessage = "Unexpected error occurred"

open class ApiException : Exception, HttpResponseProvider {
    private val response: HttpResponse<Any>

    override fun getResponse(): HttpResponse<Any> = response

    constructor(cause: Throwable) : super(cause) {
        response = HttpResponse.serverError(mapOf("message" to genericErrorMessage))
    }

    constructor(reason: String, respondWith: HttpStatus) : super(reason) {
        val messageForClient = if (respondWith == HttpStatus.INTERNAL_SERVER_ERROR) genericErrorMessage else reason
        response = HttpResponseFactory.INSTANCE
                .status<Any>(respondWith)
                .body(mapOf("message" to messageForClient))
    }
}

@Produces
@Singleton
@Requires(classes = [Exception::class, ExceptionHandler::class])
class ApiExceptionHandler() : ExceptionHandler<ApiException, HttpResponse<Any>> {

    override fun handle(request: HttpRequest<Any>, exception: ApiException): HttpResponse<Any> {
        return exception.response
    }
}

class BadRequestException(message: String) : ApiException(message, HttpStatus.BAD_REQUEST)